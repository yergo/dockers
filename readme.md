# What is `yergo/php-bundle`

The `yergo/php-bundle` is an image for Docker to ease up setting up PHP configuration into development of the project. It contains most of basic dependencies, including: ibpq, zip, libzip, unzip, libbz2, libpng, libgmp, curl and composer in addition to standard PHP images, that includes xdebug, apcu, opcache, postgres and mysql modules. So this is more or less a *docker php bundle* to easily start developing any project with help of docker.

# Using in own docker-composer

## Example `docker-compose.yml` configuration

```yaml
version: '3.1'
services:

  php:
    image: yergo/php-bundle:7.4
    ports:
      - ${PHP_PORT}:80
    volumes:
      - .:/var/www/html/
    working_dir: /var/www/html/

```

Where `${PHP_PORT}` should be configured inside `.env` file or replaced with port, we would like to access service with browser, eg. `80`.

## Shell access

```shell
docker-compose exec php /bin/bash
```

## Basic composer access
To install:
```shell
docker-compose exec php composer install
```

To update:
```shell
docker-compose exec php composer update
```

# For docker developer

## Building commands

```powershell
.\build.ps1 -docker php
.\build.ps1 php 7.4
.\build.ps1 php -version 7.4
.\build.ps1 php 7.4
.\build.ps1 php -version 7.4 -push // hub owner only available push
```

## Running ps1 scripts restricted? (Win 10)
Search "Setting for Developers", and set the box at bottomost in "Powershell" section to allow execute local scripts.

Instruction to self sign scripts [here](https://www.darkoperator.com/blog/2013/3/5/powershell-basics-execution-policy-part-1.html).

