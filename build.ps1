Param (
    [string] $docker = "php",
    [string] $version = $(Read-Host "Input $docker version" ),
    [switch] $push = $false
)

If ($push -eq $true) {
    docker push yergo/$docker-bundle:$version
} else {
    write-output "Attempt to build docker at location ./$docker/$version tagged as yergo/$docker-bundle:$version"
    docker build --squash -t yergo/$docker-bundle:$version ./$docker/$version
}